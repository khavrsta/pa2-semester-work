# Hra "Malí bombardéři se vracejí"
Autor: Stanislav Khavruk

## Téma z Progtestu

Klasická hra Bomberman

Implementujte následující varianty:

pro alespoň 2 hráče na jednom počítači
pro hru proti počítači
Hra musí splňovat následující funkcionality:

Načítání herních levelů (mapy) ze souboru (vytvořte vhodný formát)
Sbírání konfigurovatelných bonusů (zvětšení plamenu, více bomb, odpalování bomb na dálku,...)
Ukládání nejlepšího dosaženého skóre do souboru
Kde lze využít polymorfismus? (doporučené)

Ovládání hráčů: lokální hráč, umělá inteligence (různé druhy), síťový hráč
Uživatelské rozhraní: konzolové, ncurses, SDL, OpenGL (různé druhy),...
Efekty bonusů a nemocí: zvětšení plamenu, zvýšení počtu bomb,...
Další informace

https://en.wikipedia.org/wiki/Bomberman
	
## Zadání pro hru "Malí bombardéři se vracejí"

Vytvoříme klasickou hru Bomberman. Inspirovala mě hra Alawar z mého dětství s názvem “Маленькие бомберы возвращаются” (Little Bombers Return / Malí bombardéři se vracejí — je možné najít ji podle ruského nebo anglického názvu). Dole je screenshot hry od Alawar.

![Obrazvka ze hry Alawar](https://i.ytimg.com/vi/v2wDBj1Mxx8/sddefault.jpg)

Ve hře hráč se ovládá postavu Bombermana, a bude mít možnost se pohybovat okolí přes WASD nebo šipky na klávesnici a umisťovat bomby o stisknuti mezerníku.  Standartní bomba se vybuchají přes několik sekund, aby hráč měl čas odejít na bezpeční vzdalenost. 

V procesu / Na mapě hry budou:
-	Dva typy zdí, první, které hráč může zničit a druhé které nemůže (aby omezit možnost Imbalance pohybu hráče a nepřátelé)
-	Bonusy, které se mění proces hry: zvyšuje vzdálenost výbuchu, počet bomb, rychlost atd.
-	Různé druhy nepřátelé se různou logikou pochybu: jeden chodí přímo, druhý přes zeď, jinou běží jen ve stranu hráče. 

Pro dokončení hry, hráč musí zničit všech nepřátelé (později podle volného čase je možné, že implementuju entitu východu úrovně, aby hráč měl možnost získat všechny bonusy z mapy a pak přejit na následující úroveň).

### Kde mám polymorfismus?

Pro realizace hry, budou napsané různé druhy třid pro entity objektů. 

Hlavním bude `GameObject`, který shrnuje obecné parametry, jako obrazovka/textura entity, její akce (spočítaní ve virtuálně metodě `Update`), zobrazování (metoda `Render`) a pozice na mapě/okně. `GameObject` implementují `StaticGameObject` a `MoveableGameObject`. První odpovídá zeď, bomby a bonusy (Bonus a bomba budou implementovat `StaticGameObject`), a druhý odpovídá za nepřátelé a hráče. 

Kromě toho, hra bude mít možnost spustit ve konsole a SDL2 okně, a proto budou existovat dva druhy `GameTexture`: `ConsoleGameTexture` pro konzole, a `SdlGameTexture` + `AnimatedGameTexture` pro SDL2 textury. 

Díky takovému polymorfismu je možné vytvořit pole prvků `GameObject`ů (které budou mít v sobě `GameTexture`) a používat jejích metody `Update()` a `Render()` v obecném foreach ve hlavní cyklu hry.

