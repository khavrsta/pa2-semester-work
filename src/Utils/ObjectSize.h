//
// Created by Jeidoz on 07/05/2022.
//

#ifndef SRC_OBJECTSIZE_H
#define SRC_OBJECTSIZE_H

#include <cstdlib>

struct ObjectSize {
    size_t Width;
    size_t Height;

    ObjectSize();
    ObjectSize(size_t width = 0, size_t height = 0) : Width(width), Height(height) {};
    ObjectSize(const ObjectSize&);
};

#endif //SRC_OBJECTSIZE_H
