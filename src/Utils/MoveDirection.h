//
// Created by Jeidoz on 07/05/2022.
//

#ifndef SRC_MOVEDIRECTION_H
#define SRC_MOVEDIRECTION_H

// Simple abstraction of 2D move sides for AI and switches
enum class MoveDirection : char {
    None = 0,
    Up,
    Right,
    Bottom,
    Left
};

#endif //SRC_MOVEDIRECTION_H
