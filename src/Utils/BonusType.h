//
// Created by Jeidoz on 5/7/2022.
//

#ifndef SRC_BONUSTYPE_H
#define SRC_BONUSTYPE_H

// Currently, I am not sure should I use polymorphism for all bonuses
// Or simply implement ApplyBonus(BonusType, Character) for BonusObject
enum class BonusType : char {
    None = 0,
    BombRange,
    BombAmount,
    Life,
    Rollers,
    WallWalking,
    FireProofShield
};

#endif //SRC_BONUSTYPE_H
