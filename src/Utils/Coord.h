//
// Created by Jeidoz on 07/05/2022.
//

#ifndef SRC_COORD_H
#define SRC_COORD_H

#include <cstdlib>

struct Coord {
    size_t _x, _y;
    Coord(size_t x = 0, size_t y = 0) : _x(x), _y(y) {}
};

#endif //SRC_COORD_H
