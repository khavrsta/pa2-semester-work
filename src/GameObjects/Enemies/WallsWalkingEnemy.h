//
// Created by Jeidoz on 5/8/2022.
//

#ifndef SRC_WALLSWALKINGENEMY_H
#define SRC_WALLSWALKINGENEMY_H

#include <memory>
#include "Enemy.h"
#include "../GameObject.h"

// those enemies can walk through walls
class WallsWallingEnemy : Enemy {
public:
    WallsWallingEnemy();
    void Update() override;
    void Render() override;
    bool IsCollidesWith(const GameObject& obj) const override;
    shared_ptr<GameObject> GetShared() const override;
    virtual void Die() override;
};

#endif //SRC_WALLSWALKINGENEMY_H
