//
// Created by Jeidoz on 5/7/2022.
//

#ifndef SRC_ENEMY_H
#define SRC_ENEMY_H

#include <memory>
#include "../MovableGameObject.h"
#include "../GameObject.h"

using std::shared_ptr;

// enemy class skeleton with dedicated logic
class Enemy : public MovableGameObject {
private:
protected:
    virtual void Move() override;
    void KillPlayer(const Character&); // basic method for removing 1 life
    virtual void Die() override; // play death animation
public:
    Enemy();
    void Update() override;
    void Render() override;
    // does not collide with other enemies
    bool IsCollidesWith(const GameObject& obj) const override;
    shared_ptr<GameObject> GetShared() const override;
};

#endif //SRC_ENEMY_H
