//
// Created by Jeidoz on 5/8/2022.
//

#ifndef SRC_STRAIGHTFORWARDENEMY_H
#define SRC_STRAIGHTFORWARDENEMY_H

#include <memory>
#include "Enemy.h"
#include "../GameObject.h"

// would implement simple logic to move forward until met wall
// when met, change direction (preferable opposite)
class StraightForwardEnemy : Enemy {
public:
    StraightForwardEnemy();
    void Update() override;
    void Render() override;
    bool IsCollidesWith(const GameObject& obj) const override;
    shared_ptr<GameObject> GetShared() const override;
    virtual void Die() override;
};

#endif //SRC_STRAIGHTFORWARDENEMY_H
