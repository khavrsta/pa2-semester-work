//
// Created by Jeidoz on 5/8/2022.
//

#ifndef SRC_HUNTERENEMY_H
#define SRC_HUNTERENEMY_H

#include <memory>
#include "Enemy.h"
#include "../GameObject.h"

// those enemies would try to move to direction of player
class HunterEnemy : Enemy {
public:
    HunterEnemy();
    void Update() override;
    void Render() override;
    bool IsCollidesWith(const GameObject& obj) const override;
    shared_ptr<GameObject> GetShared() const override;
    virtual void Die() override;
};

#endif //SRC_HUNTERENEMY_H
