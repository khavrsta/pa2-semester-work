//
// Created by Jeidoz on 07/05/2022.
//

#ifndef SRC_STATICGAMEOBJECT_H
#define SRC_STATICGAMEOBJECT_H

#include "GameObject.h"

// represents breakable and unbreakable walls, bonuses, bombs, etc.
class StaticGameObject : public GameObject {
protected:
    bool _isAlreadyRendered; // for evading multiple texture renders i.e. for walls/bonuses...
    virtual void OnCollision();
public:
    StaticGameObject();
    StaticGameObject(const StaticGameObject& obj);
    virtual void Update() override;
    virtual void Render() override;
    virtual bool IsCollidesWith(const GameObject&) const override;
    virtual shared_ptr<GameObject> GetShared() const override;
};

#endif //SRC_STATICGAMEOBJECT_H
