//
// Created by Jeidoz on 5/7/2022.
//

#ifndef SRC_BONUSOBJECT_H
#define SRC_BONUSOBJECT_H

#include <memory>
#include "../Utils/BonusType.h"
#include "StaticGameObject.h"

using std::shared_ptr;

class BonusObject : public StaticGameObject {
private:
    BonusType _type;
    bool _isHidden;
public:
    BonusObject();
    BonusObject(const BonusObject&);

    void Update() override;
    void Render() override;
    //should collide only with player
    bool IsCollidesWith(const GameObject& obj) const override;
    shared_ptr<GameObject> GetShared() const override;
    BonusType GetType() const;
    void ApplyBonus(const Character&) const;
};

#endif //SRC_BONUSOBJECT_H
