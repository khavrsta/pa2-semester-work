//
// Created by Jeidoz on 5/7/2022.
//

#ifndef SRC_CHARACTER_H
#define SRC_CHARACTER_H

#include <memory>
#include <string>
#include "GameObject.h"
#include "MovableGameObject.h"
#include "Bomb.h"
using std::string, std::shared_ptr;

class Character : public MovableGameObject {
private:
    bool _hasIncreaseSpeed; // need for rendering speed bonus icon?
    bool _hasWallWalking; // ignore breakable walls
    bool _isFireproof; // ignore explosions
    string _name; // for score table
    size_t _leftLifes;
    unique_ptr<Bomb> _playerBomb; // saves bomb instance (each player can have unique set of bomb characteristics)
protected:
    void Move() override;
    void Die() override;
public:
    Character();
    Character(const Character&);
    void Update() override;
    void Render() override;
    bool IsCollidesWith(const GameObject& obj) const override;
    shared_ptr<GameObject> GetShared() const override;
    void OnKeyPressed();
};

#endif //SRC_CHARACTER_H
