//
// Created by Jeidoz on 5/7/2022.
//

#ifndef SRC_BOMB_H
#define SRC_BOMB_H

#include <memory>
#include "Character.h"
#include "StaticGameObject.h"

using std::shared_ptr;

class Bomb : public StaticGameObject {
private:
    static const DefaultExplosionTime = 3; // 3 seconds

    size_t _range;
    size_t _timeLeft; // ... before explosion
    shared_ptr<Character> _owner; // for multiple players
    void Explode();
public:
    // ctors automatically sets timer
    Bomb();
    Bomb(Coords position);
    void Update() override;
    void Render() override;
    shared_ptr<GameObject> GetShared() const override;
    bool IsCollidesWith(const GameObject&) const override;
    void SetRange(size_t newRange);
    size_t GetRange() const;
    void Place(Coord);
};

#endif //SRC_BOMB_H
