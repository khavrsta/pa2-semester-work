//
// Created by Jeidoz on 07/05/2022.
//

#ifndef SRC_GAMEOBJECT_H
#define SRC_GAMEOBJECT_H

#include <memory>
#include "../Utils/Coord.h"
#include "../Graphics/GameTexture.h"
using std::shared_ptr;

class GameObject {
protected:
    Coord _position; // where on screen it is placed
    GameTexture _texture; // what to display
public:
    GameObject();
    GameObject(const GameObject&);
    virtual void Update(); // process logic operations, like movement or dmg dealing
    virtual void Render(); // displays all graphic elements on the display
    Coord GetPosition() const;
    virtual bool IsCollidesWith(const GameObject&) const; // virtual Collision checker
    virtual shared_ptr<GameObject> GetShared() const;
    // later here may be event methods like OnDestroy(), InvokeDestroy() or smth of it.
};

#endif //SRC_GAMEOBJECT_H
