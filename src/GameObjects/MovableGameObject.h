//
// Created by Jeidoz on 07/05/2022.
//

#ifndef SRC_MOVABLEGAMEOBJECT_H
#define SRC_MOVABLEGAMEOBJECT_H

#include <memory>
#include <vector>
#include "GameObject.h"
#include "../Utils/MoveDirection.h"

using std::vector, std::shared_ptr;

// represents player, enemies, bullets?
class MovableGameObject : public GameObject {
protected:
    size_t _speed;
    shared_ptr<GameTexture> _deathTexture;
    virtual void Move(); // process object moving
    virtual void Die(); // play death animation
public:
    MoveableGameObject();
    MoveableGameObject(const MoveableGameObject&);
    virtual void Update() override;
    virtual void Render() override;
    virtual bool IsCollidesWith(const GameObject&) const override;
    virtual shared_ptr<GameObject> GetShared() const override;
};

#endif //SRC_MOVABLEGAMEOBJECT_H
