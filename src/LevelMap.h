//
// Created by Jeidoz on 07/05/2022.
//

#ifndef SRC_LEVELMAP_H
#define SRC_LEVELMAP_H

#include <cstdlib>
#include <vector>
#include <string>
#include <fstream>
#include <memory>
#include "GameObjects/GameObject.h"
#include "Utils/ObjectSize.h"

using std::size_t, std::vector, std::string, std::ifstream, std::shared_ptr;

class LevelMap {
private:
    ObjectSize _mapSize;
    vector<shared_ptr<GameObject>> _gameObjects;
public:
    LevelMap();
    void Load(const string& fileName);
    void Save(const string& fileName);
    void Generate(const string& seed); // generate level by level seed

    // may be those methods would be replaced by event system (add/destroy on GameObject event)
    void AddGameObject(const GameObject& obj); // object already contains coordinates
    void Destroy(const GameObject& obj); // remove from map/game object instance
};

#endif //SRC_LEVELMAP_H
