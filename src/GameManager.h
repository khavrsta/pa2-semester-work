//
// Created by Jeidoz on 5/7/2022.
//

#ifndef SRC_GAMEMANAGER_H
#define SRC_GAMEMANAGER_H

#include <cstdlib>
#include <string>
#include <memory>
#include "LevelMap.h"

using std::string, std::unique_ptr;

class GameManager {
private:
    bool _isInitialized;
    bool _isRunning; // is not paused
    unique_ptr<LevelMap> _map;
    size_t _gameTime;
public:
    GameManager();

    bool IsWon(); // is game/level won
    bool IsRunning(); // game is not paused
    void Load(const string& filename); // load entire game state
    void Save(const string& filename); // save entire game state
    LevelMap& GetMap();
    size_t GetTime();
};

#endif //SRC_GAMEMANAGER_H
