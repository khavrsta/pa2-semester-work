//
// Created by Jeidoz on 5/7/2022.
//

#ifndef SRC_APPLICATION_H
#define SRC_APPLICATION_H

#include <memory>
#include "GameManager.h"
#include "LevelMap.h"

using std::unique_ptr;

// will manage entire window and render fields from SDL2
class Application {
private:
    unique_ptr<GameManager> _state;
    unique_ptr<LevelMap> _currentLevel;
    string _saveFilePath;
    static unique_ptr<GameManager> _gameManager;
public:
    explicit Application();

    void Run();
    void Close();
    bool IsRunning(); // is application not closed
};

#endif //SRC_APPLICATION_H
