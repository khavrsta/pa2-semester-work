//
// Created by Jeidoz on 5/8/2022.
//

#ifndef SRC_SDLGAMETEXTURE_H
#define SRC_SDLGAMETEXTURE_H

#include <memory>
#include <string>
#include "GameTexture.h"

using std::string, std::shared_ptr;

class SdlGameTexture : GameTexture {
private:
    // some SDL2 variables for sprite and images
public:
    SdlGameTexture();
    SdlGameTexture(const string& filePath); // load texture
    void Draw(const Coord& position) override;
    shared_ptr<GameTexture> GetShared() const override;
};

#endif //SRC_SDLGAMETEXTURE_H
