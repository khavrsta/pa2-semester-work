//
// Created by Jeidoz on 5/8/2022.
//

#ifndef SRC_ANIMATEDGAMETEXTURE_H
#define SRC_ANIMATEDGAMETEXTURE_H

#include <memory>
#include "SdlGameTexture.h"

using std::shared_ptr;

class AnimatedGameTexture : SdlGameTexture {
private:
    // SDL2 type fields for saving animated sprites
public:
    AnimatedGameTexture();
    AnimatedGameTexture(const string& filePath);
    // SDL2 methods for render
    void Draw(const Coord& position) const override;
    shared_ptr<GameTexture> GetShared() const override;
};

#endif //SRC_ANIMATEDGAMETEXTURE_H
