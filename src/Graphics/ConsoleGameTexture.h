//
// Created by Jeidoz on 07/05/2022.
//

#ifndef SRC_CONSOLEGAMETEXTURE_H
#define SRC_CONSOLEGAMETEXTURE_H

#include <memory>
#include "GameTexture.h"

using std::shared_ptr;

// not sure about Multiple interfaces for rendering.
// I assume that game mb would be in SDL2 and this class would not implement
class ConsoleGameTexture : GameTexture {
private:
    char _textureCharacter;
public:
    ConsoleGameTexture();
    ConsoleGameTexture(const ConsoleGameTexture&);
    ConsoleGameTexture(char symbol);
    void Draw(const Coord& position) const override;
    shared_ptr<GameTexture> GetShared() const override;
};

#endif //SRC_CONSOLEGAMETEXTURE_H
