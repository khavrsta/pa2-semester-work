//
// Created by Jeidoz on 07/05/2022.
//

#ifndef SRC_GAMETEXTURE_H
#define SRC_GAMETEXTURE_H

#include <memory>
#include "../Utils/ObjectSize.h"
#include "../Utils/Coord.h"

using std::shared_ptr;

class GameTexture {
protected:
    ObjectSize _size;
public:
    GameTexture();
    GameTexture(const GameTexture&);
    // Each GameObject must have Texture, therefore position is GameObject::_position
    // but not each texture should be only in GameObject (i.e. Level background cannot be game object — just image)
    virtual void Draw(const Coord& position) const;
    virtual shared_ptr<GameTexture> GetShared() const;
    ObjectSize GetSize() const;
};

#endif //SRC_GAMETEXTURE_H
